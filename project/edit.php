<?php

    require 'config.php';
    require 'blog.php';
    $bt = discat($conn);
    $idval=$_GET["eid"];
    $row = getblogbyid($conn,$idval);
    $data2 = gettagsbybid($idval,$conn);
   
             
             
?>
 
<!DOCTYPE html>
<html lang="en">
 
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
 
  <title>Clean Blog - Start Bootstrap Theme</title>
 
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
 
  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
 
  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
 
</head>
 
<body>
 
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="managecat.php">MANAGE CATEGORY</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
 
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/blog-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Add your blog</h1>
            <span class="subheading">Lets start blogging...</span>
          </div>
        </div>
      </div>
    </div>
  </header>
 
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p>Fill out your Name and text on the below form.</p>
       
        <form name="blogform" method="POST">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Title</label>
              <input type="text" class="form-control" value="<?php echo $row["title"]; ?>" name="title" required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Blog</label>
              <textarea rows="10" class="form-control" name="blog" required data-validation-required-message="Please type your content."><?php echo $row["content"]; ?></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Tags</label>
              <textarea rows="3" class="form-control" placeholder="Tags" name="tags" required data-validation-required-message="Please type your content."><?php
              if (isset($data2)) {
                foreach ($data2 as $row2) {
                  echo $row2["tags"].",";        
                }
              }
                ?>
            </textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <br>
          <div class="container">
          <div class="row">
         <div class="col-lg-8 col-md-10 mx-auto">
          <div class="btn btn-primary" >


          <select id="selectcategory" name='sel1[]' multiple="multiple" size=6>
         <option value="" disabled>--Select Category--</option>
          <?php foreach($bt as $row){
          $cid = $row['cid'];
          $res = category_selected($conn,$idval,$cid);
          $count = $res->rowCount();
          echo '<label for="one"> ';
          if ($count == 0) {
             echo '<option value="'.$row["catog"].'"  >'.$row["catog"].'</option>';
          } else {               
            echo '<option value="'.$row["catog"].'"  selected="selected">'.$row["catog"].'</option>';
          }
        }
          ?>                
           </select>



          <!-- <select  name="sel1[]" multiple="multiple" >
         <option value="">select categories</option>
         <?php //

       //foreach($bt as $row)
       //{   ?>
      <option value="<?php// echo $row["catog"];?>"> <?php //echo $row["catog"];?>  </option>
      <?php //}?>
          </select>
  --> 

      </div>
      <hr>
          <div id="success"></div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name= "addblog" id="AddBlogButton">update</button>
          </div>
        </form>
<?php
require 'config.php';
 
if(isset($_POST['addblog'])) {
  if(isset($_POST['title'])){
    $title = $_POST['title'];
   }
  if(isset($_POST['blog'])) {
    $blogg = $_POST['blog'];
   }
   if(isset($_POST['tags'])) {
    $tag = $_POST['tags'];
   }
   if (isset($_POST['sel1'])) {
    $op1 = $_POST['sel1'];
  }

 
   $currentdate = date("Y-m-d H:i:s");
   $tag = strtolower($tag);
   $words = explode(",",$tag);
   $s = sizeof($words);
   $s1 = sizeof($op1);
 
   for($i=0;$i<$s;$i++) {
    $words[$i]=trim($words[$i]);
   }
 
   try {  
     $blogg = addslashes($blogg);
     upblog($title,$blogg,$idval,$conn);
     deljun($idval,$conn);
     delcatjun($idval,$conn);
     insblogtag($words,$title,$s,$conn);
     inscatjun($op1,$title,$s1,$conn);
     echo "Updated successfully";
     header("location:index.php"); 
   }
   catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
   }
  
   }
 
?>
 
      </div>
    </div>
  </div>
 
  <hr>
 
  
 
  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
 
 
  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
 
</body>
 
</html>