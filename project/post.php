<!DOCTYPE html>
<html lang="en">

<head>


  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="managecat.php">MANAGE CATEGORY</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/post-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
              <?php
              require 'config.php';
              require 'blog.php';
              $idval = $_GET["id"];
              $val = $_GET["id"];
              $row = getblogbyid($conn,$idval);
              if (isset($row)) {
                 echo '
                      <h1>'.$row["title"].'</h1>
                      <span class="meta">Posted by
                      <a href="#">Start Bootstrap</a>
                      on '.$row["date"].'</span>
                      <div class="container">
                      <div class="row">
                     
                    ';
          
              }
              else {
                echo "0 results";
              }
                           
              
              ?>

            
          </div>
        </div>
      </div>
    </div>
  </header>

  <!--Post Content -->
  <<article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
       
        <p><?php echo $row["content"];
                ?></p>
             
            
                <p>Related tags: 
                <?php
                
                $data2 = gettagsbybid($idval,$conn);
                echo "<p>Tags: ";
                if (isset($data2)) { 
                  foreach ($data2 as $row2) {
                    $tagidval = $row2["tid"];
                    echo '<a href="relatedposts.php?tag='.$tagidval.'">#'.$row2["tags"].' </a>';
                        
                  }
                }
                $dt1 = getcatbybid($val,$conn); 
                echo "<p>category: ";
                if (isset($dt1)) {
                    foreach ($dt1 as $row3) {
                        $tagidval1 = $row3["cid"];
                       
                        echo '<a href="relatedposts.php?category='.$tagidval1.'">#'.$row3["catog"].' </a>';
                        
                    }
                }
              
                 ?>
                 <div class="clearfix">
                <ul class="pagination">
                <li  class="">
                <a class="btn btn-primary float-right" href="edit.php?eid=<?php echo $idval?>">edit </a>
                </li>
                <br>
               
               <li  class="">
                <a class="btn btn-primary float-right" href="delete.php?eid=<?php echo $idval?>" onclick="return confirm ('are you sure want to delete')">delete </a>
                </li>
                 </ul>
                </div>
              </p>
                </div>
                 <hr>


               
                
              </p>
        
        </div>
      </div>
    </div>
  </article>

  <hr>

 

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
